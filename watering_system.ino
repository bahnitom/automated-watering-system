#include <Adafruit_SSD1306.h>
#include <Wire.h>
#include <WiFiClient.h>
#include <BlynkSimpleEsp32.h>

#define sensor 33
#define relay 4

#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 64

// Declaration for SSD1306 display connected using I2C
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);

/* Blynk set up */
// Enter your Auth token
char auth[] = "******";
// Enter your WIFI SSID and password
char ssid[] = "******";
char pass[] = "******";
// This function creates the timer object. It's part of Blynk library
BlynkTimer timer;

void setup() {
  // Debug console
  Serial.begin(115200);
  Blynk.begin(auth, ssid, pass, "blynk.cloud", 80);

  // Initialize the SSD1306 display with address 0x3C
  if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {
    Serial.println("SSD1306 allocation failed");
    for (;;)
      ;
  }

  // Display loading screen
  display.clearDisplay();
  display.setTextSize(2);
  display.setTextColor(SSD1306_WHITE);
  display.setCursor(0, 0);
  display.println("System Loading");
  display.display();

  pinMode(relay, OUTPUT);
  digitalWrite(relay, HIGH);
  delay(2000);
}

// Get the soil moisture sensor values
void soilMoisture() {
  int value = analogRead(sensor);
  value = map(value, 0, 4095, 0, 100);
  value = (value - 100) * -1;
  Blynk.virtualWrite(V0, value);
  Serial.println(value);

  // show moisture value
  display.clearDisplay();
  display.setTextSize(2);
  display.setTextColor(SSD1306_WHITE);
  display.setCursor(0, 0);
  display.print("Vlhkost je     ");
  display.print(value);
  display.print(" ");
  display.display();
}

//Get the button value
BLYNK_WRITE(V1) {
  bool Relay = param.asInt();
  if (Relay == 1) {  // button is on
    digitalWrite(relay, LOW);
    display.clearDisplay();
    display.setTextSize(2);
    display.setTextColor(SSD1306_WHITE);
    display.setCursor(0, 0);
    display.print(" Motor je zapnut");
    display.display();
    delay(2000);
  } else {  // button is off
    digitalWrite(relay, HIGH);
    display.clearDisplay();
    display.setTextSize(2);
    display.setTextColor(SSD1306_WHITE);
    display.setCursor(0, 0);
    display.print(" Motor je vypnut");
    display.display();
    delay(2000);
  }
}

void loop() {
  soilMoisture();
  Blynk.run();  // Run the Blynk library
  timer.run();  // run BlynkTimer

  delay(200);
}
